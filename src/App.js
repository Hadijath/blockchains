import './App.css';
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import './index.css';
import axios from 'axios';

//const e = React.createElement;

class NameForm extends React.Component {
  
  state = {
    amount: '',
    token:'',
    numero:'',
    message:'',
    isWaiting: false,
    returnUrl :''
  }

  componentDidMount(){
    var ReverseMd5 = require('reverse-md5');
    var rev = ReverseMd5({
      lettersUpper: false,
      lettersLower: true,
      numbers: true,
      special: false,
      whitespace: true,
      maxLen: 12
  });

    var str = window.location.href;
    var url = new URL(str);
    this.setState({isWaiting: false});
       
    var price = '';
    var token ='';

    if(url.search.includes('token='))
    {
      var data=url.search.split('&');
      if(data.length>0)
      {
        var tab = data[0].split('=');
        price = tab[1];
        price = rev(price);
        price = price['str'];
        tab = data[1].split('=');
        token = tab[1];
       
        this.setState({amount: price});
        this.setState({token: token});
      }
    }

    let returnLink = document.getElementById("returnLink");
    returnLink.style.display="none";

}

  handleChange = event => {
    this.setState({ numero: event.target.value });
  }

  handleSubmit = event => {
    event.preventDefault();
    var msg = document.getElementById("message_request");

    if (!this.state.numero){
      msg.textContent = "Numéro invalide";
      msg.classList.add('alert-warning');
      return
    }
    var loader = document.getElementById('loader');
    let pay = document.getElementById("pay");    

    loader.classList.add('loader');
    
    loader.classList.add('active');
    pay.style.display="none";
    msg.textContent = "Transaction en cours";
    msg.classList.add('alert-warning');
    this.setState({isWaiting: true});
   
    var data = JSON.stringify(
      {
        "recipient":"string",
        "amount":0,
        "sender":"string",
        "externalId":"string",
        "partyId":"string"
        }
    );
    
    var config = {
      method: 'post',
      url: 'https://soutenance-app.herokuapp.com/blockchain/all_transaction',
      headers: { 
        'accept': 'application/json', 
        'Content-Type': 'application/json'
      },
      data : data
    };
    
    axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
    })
    .catch(function (error) {
      console.log(error);
    });
    /*axios({
      method: 'post',
      url: 'https://soutenance-app.herokuapp.com/blockchain/all_transaction',
      headers: {
          'accept': 'application/json',      
          'Content-Type': 'application/json'   
        },
      data: {
          "amount": this.state.amount,
          "currency": "EUR",
          "externalId": "1234",
          "partyId": this.state.numero,
          "recipient": "",
          "sender": ""
      }
    }).then(json_response => { 
      this.setState({returnUrl: 'http://www.domaineweb.xyz/fr/module/paymentexample/validationAPI?status='+'&token='+this.state.token});
      this.setState({message: json_response.data.message})
      console.log(json_response.data.message); 
      loader.classList.remove('loader');
      msg.classList.remove('alert-warning');
      msg.classList.add('alert-success');
      msg.textContent = "Paiement réussi";
      let returnLink = document.getElementById("returnLink");
      returnLink.style.display="block";
      pay.style.display="none";*/
     /* this.setState({isWaiting: false});
      // this.setState({returnUrl: 'https://lesgrandesaffaires.com/fr/module/paymentexample/validationAPI?status='+status+'&token='+this.state.token});
      this.setState({returnUrl: 'http://www.domaineweb.xyz/fr/module/paymentexample/validationAPI?status='+status+'&token='+this.state.token});
      var status = res.status;
      console.log(res.status); 
      //loader.classList.remove('loader');
      loader.classList.remove('loader');
      msg.classList.remove('alert-warning');
      msg.classList.add('alert-danger');
      msg.textContent = "Echec du paiement";
      if(status == '202'){
        msg.classList.remove('alert-danger');
        msg.classList.add('alert-success');
        msg.textContent = "Paiement réussi";
      }
    let returnLink = document.getElementById("returnLink");
    returnLink.style.display="block";
    pay.style.display="none";*/
      //document.location.href='https://lesgrandesaffaires.com/fr/module/paymentexample/validationAPI?status='+status+'&token='+this.state.token;

   // }) 
   
  } 
 

  render() {
    return (
      
      <div class="container">
        
        <div class="columns is-4">
          <form  onSubmit={this.handleSubmit} class="box"> 
            <h1 class="is-size-2 has-text-weight-bold">Paiement par Mobile Money</h1><hr></hr>
            <div class="field">
              <label class="label mt-6 mb-3 has-text-weight-normal">Numéro de téléphone</label>
              <div class="control">
                <input class="input is-medium is-primary" type="tel" placeholder="Numéro de téléphone" name="numero" onChange={this.handleChange}/>
              </div>
            </div>
            <div class="field">
              <label class="label mb-3 has-text-weight-normal">Montant</label>
              <div class="control">
              <input class="input is-medium is-primary" type="number" value= {this.state.amount}  id="amount"/>
              </div>
            </div>
            <div class="field">
              <div class="control">
                <button class="button is-success is-light is-medium is-rounded" type="submit" id="pay" class="pay" disabled={this.state.isWaiting == true ? true : false  }><p class="is-size-4 has-text-weight-semibold has-text-centered">Payer</p></button>
            
              </div>
            </div>
            <div id="loader">
              {this.state.message}
            </div>
            <a class="button" id="returnLink"  href="http://domaineweb.xyz/fr/">Retourner à la boutique</a>

            
              <a class="button" href="http://domaineweb.xyz/fr/">Retourner à la boutique</a>
             <br></br>
            <div class="alert" id="message_request" role="alert"></div>

          </form>
        </div>
        </div>




          
    );
  }
}

 

export default NameForm;
